import xlrd as xrl
import os
import sys
import matplotlib


def retrieveLabels(name):
    s = name.split("_")
    return s[0], s[4].split(".")[0]



loc = ("dataset/")

def main():
    # To open Workbook
    file_list = os.listdir(loc)
    values = {}
    for f in file_list:
        wb = xrl.open_workbook(loc + f)

        department, label = retrieveLabels(f)
        values[department] = []
        print("Parsing " + f)
        for s in wb.sheet_names():
            if "TRAINING" in s:
                continue
            xl_sheet = wb.sheet_by_name(s)
            num_cols = xl_sheet.ncols   # Number of columns
            for row_idx in range(1, xl_sheet.nrows):    # Iterate through rows
                temp_row = xl_sheet.row_slice(row_idx,0,21)
                temp = []
                for item in temp_row:
                    temp.append(item.value)
                # Converting Categorical value to Numeric
                if label == "Right":
                    temp.append(1)
                elif label == "Wrong":
                    temp.append(0)
                else:
                    print("Something went wrong")
                values[department].append(temp)

    print(values)


if __name__ == "__main__":
    main()
