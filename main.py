import xlrd as xrl
import os
import sys
import numpy as np
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from sklearn import preprocessing, neighbors, svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score
from keras.callbacks import ModelCheckpoint

from keras.models import Sequential
from keras.utils import np_utils
from keras.layers import Dense, Dropout, GaussianNoise, Conv1D
from keras.preprocessing.image import ImageDataGenerator

import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.metrics import accuracy_score


def retrieveLabels(name):
    s = name.split("_")
    return s[0], s[4].split(".")[0]



loc = ("dataset/")

def main():
    # ------
    # Reading excels
    file_list = os.listdir(loc)
    values = []
    for f in file_list:
        wb = xrl.open_workbook(loc + f)
        department, label = retrieveLabels(f)
        print("Parsing " + f)
        for s in wb.sheet_names():
            if "TRAINING" in s:
                continue
            xl_sheet = wb.sheet_by_name(s)
            num_cols = xl_sheet.ncols   # Number of columns
            headers = xl_sheet.row_slice(0,0,21)
            teste_h = []
            for h in headers:
                teste_h.append(h.value)
            teste_h.append("label")
            teste_h.append("department")
            for row_idx in range(1, xl_sheet.nrows):    # Iterate through rows
                temp_row = xl_sheet.row_slice(row_idx,0,21)
                temp = []
                for item in temp_row:
                    temp.append(item.value)
                # Converting Categorical values to Numeric
                # Label
                if label == "Right":
                    temp.append(1)
                elif label == "Wrong":
                    temp.append(0)
                else:
                    print("Unknown label: " + label)
                # Department
                if department == "esec":
                    temp.append(0)
                elif department == "DEI":
                    temp.append(1)
                else:
                    print("Unknown department: " + department)
                values.append(tuple(temp))

    # ------
    # Creating data frames
    df = pd.DataFrame.from_records(values, columns=teste_h)

    # ------
    # Pre-processing
    # Statistics
    print(df)
    d = df['department'].value_counts()
    print(d)
    l = df['label'].value_counts()
    print(l)
    df = df.replace("", np.nan)


    # Missing values plot
    total = df.isna().sum().sort_values(ascending=False)
    percent = (df.isna().sum()/df.isna().count()).sort_values(ascending=False)
    missing_data = pd.concat([total, percent], axis=1, keys=['Total', 'Percent'])
    f, ax = plt.subplots(figsize=(15, 6))
    plt.xticks(rotation='90')
    sns.barplot(x=missing_data.index, y=missing_data['Percent'])
    plt.xlabel('Features', fontsize=15)
    plt.ylabel('Percent of missing values', fontsize=15)
    plt.title('Percent missing data by feature', fontsize=15)
    missing_data.head()
    #plt.show() #TODO: Remove comment

    # ----
    # Strategy to deal with missing data https://towardsdatascience.com/handling-missing-values-in-machine-learning-part-1-dda69d4f88ca
    df.dropna(inplace=True) # We decided to take the rows that contain "Not A Number" values strategy since they comprise a small portion of the dataset

    # -----
    # Data splitting
    LABEL = 'label'
    #LABEL = 'department'
    train, test = np.split(df.sample(frac=1), [int(.8*len(df))])
    print("Size of train data: " + str(len(train)))
    print("Size of train data: " + str(len(test)))
    Y_train = train[LABEL].values.astype('int32')
    Y_test = test[LABEL].values.astype('int32')

    train.drop([LABEL], axis=1, inplace=True)
    test.drop([LABEL], axis=1, inplace=True)
    X_train = (train.values).astype('float32')
    X_test = (test.values).astype('float32')

    # ------
    # PCA Analysis # https://www.kaggle.com/pmmilewski/pca-decomposition-and-keras-neural-network

    # Centering
    scaler = StandardScaler()
    scaler.fit(X_train)
    X_sc_train = scaler.transform(X_train)
    X_sc_test = scaler.transform(X_test)

    # Covariance analysis
    pca = PCA(n_components=22)
    pca.fit(X_train)
    plt.plot(np.cumsum(pca.explained_variance_ratio_))
    plt.xlabel('Number of components')
    plt.ylabel('Cumulative explained variance')
    #plt.show()

    NCOMPONENTS = 10 # TODO: Change here as required, we picked 5 cause it keeps 0.978 of the the cumulative variance

    pca = PCA(n_components=NCOMPONENTS)
    X_pca_train = pca.fit_transform(X_sc_train)
    X_pca_test = pca.transform(X_sc_test)
    pca_std = np.std(X_pca_train)

    print(X_pca_train.shape)

    # -----
    # Neural Network
    model = Sequential()
    layers = 5
    UNITS_1 = 10
    UNITS_2 = 10
    mp = 100
    if LABEL == "label":
        class_weight = {0: mp* (1-(l[0]/(l[0] + l[1]))), 1: mp*(1-(l[1]/(l[0] + l[1])))}
    elif LABEL == "department":
        class_weight = {0: mp* (1-(d[0]/(d[0] + d[1]))), 1: mp*(1-(d[1]/(d[0] + d[1])))}
    print(class_weight)
    model.add(Dense(UNITS_1, input_dim=NCOMPONENTS, activation='relu'))
    model.add(GaussianNoise(pca_std))
    for i in range(layers):
        model.add(Dense(UNITS_2, activation='relu'))
        model.add(GaussianNoise(pca_std))
        model.add(Dropout(0.2))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    #model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['categorical_accuracy'])
    history = model.fit(X_pca_train, Y_train, epochs=1000, class_weight=class_weight, batch_size = 250, validation_split=0.15, verbose=2)

    # Prediction
    predictions = model.predict_classes(X_pca_test, verbose=0)
    #print(predictions)
    # Evaluating the prediction TODO: Put various metrics taught in the classroom
    nn_a = accuracy_score(predictions,Y_test)


    # ----
    # SVM Analysis || Based off: https://pythonprogramming.net/support-vector-machine-intro-machine-learning-tutorial/
    clf = svm.SVC()
    clf.fit(X_pca_train, Y_train)
    svm_a = clf.score(X_pca_test, Y_test)

    # ----
    # Decision trees analysis || Based off: https://blog.goodaudience.com/introduction-to-random-forest-algorithm-with-python-9efd1d8f0157
    parameters = {'bootstrap': True,
              'min_samples_leaf': 3,
              'n_estimators': 1000  ,
              'min_samples_split': 10,
              'max_features': 'sqrt',
              'max_depth': 6,
              'max_leaf_nodes': None}

    RF_model = RandomForestClassifier(**parameters)
    RF_model.fit(X_pca_train, Y_train)
    RF_predictions = RF_model.predict(X_pca_test)
    rf_a = accuracy_score(RF_predictions, Y_test)


    # ----
    # Final Results
    print("---------------------------------------")
    print('Neural Network Accuracy is:', nn_a*100) #TODO: Accuracy está baixa como tudo, adaptar o modelo
    print("SVM Accuracy is:", svm_a*100)
    print("Random Forest Accuracy is:", rf_a*100)

if __name__ == "__main__":
    main()
